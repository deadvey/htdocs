projects = [
    ["Falling (game)","A game terminal based game where a character falls down a pit and you need to dodge obsticles, infinite runner","C++","gcc<br/><i>run g++ falling.cpp -o falling</i> to compile","July 2024","falling.zip","falling.tar.gz"],
    ["Binary Clock","A clock that counts using binary digits, see <a href='https://en.wikipedia.org/wiki/Binary_clock'>The Wiki Article</a> for more info", "Python","-python<br/>-A braille supporting font","May 2024","binclock.zip","binclock.tar.gz"],
    ["Terminal Snake","A simpile Snake game written in Python","Python","-python","April-May 2024","snake.zip","snake.tar.gz"],
    ["screenshot","A simple Linux tool for taking a screenshot in a rectangular region, copying it and saving it to a directory","Bash (Shell)","-Bash shell<br/>For X Tool:<br/>-maim<br/>-xclip<br/>For Wayland Tool:<br/>-grim<br/>-wl-clipboard<br/>-slurp","October 2023","screenshot.zip","screenshot.tar.gz"]
]


if (typeof module !== 'undefined' && module.exports) {
	module.exports = projects;
}
